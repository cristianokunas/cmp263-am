# Explainability and interpretability of decision trees and artificial neural networks: a comparison in an I/O Access Pattern Detector

Andressa S. da Silva<sup>1</sup>, Bianca M. de Barros<sup>2</sup>, Cristiano A. Künas<sup>1</sup>

> <sup>1</sup> Institute of Informatics, Federal University of Rio Grande do Sul (UFRGS), Av. Bento Gonçalves, 9500 - Porto Alegre, Brazil
> 
> <sup>2</sup> State Center for Research in Remote Sensing and Meteorology, Federal University of Rio Grande do Sul (UFRGS), Av. Bento Gonçalves, 9500 - Porto Alegre, Brazil

This repository contains the datasets, source-codes, and parsing scripts from our paper: 

https://cristianokunas.gitlab.io/cmp263-am/
